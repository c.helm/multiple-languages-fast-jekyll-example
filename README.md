helm-nagel.com
===============

Homepage of Helm & Nagel GmbH. 


Contribution
------------

Jekyll theme based on [Freelancer bootstrap theme ](http://startbootstrap.com/templates/freelancer/)

## Install correct jekyll version

    `$ gem install jekyll -v 3.8.5`
    
If "bundle" cannot be imported, try to run `bundle install`

## Run Helm & Nagel Homepage

    `jekyll serve`
    
## Exclude content from beeing uploaded to the homepage

Create a folder `_yourfoldername` with a underscore and jekyll does not copy 
the content of this folder.

Gitlab.com artifact limit is 1GB: https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlab-cicd

## Run the Konfuzio Homepage

- For more details, read the [jekyll documentation](http://jekyllrb.com/)

## Make Great SVG

- https://ivanceras.github.io/svgbob-editor/

## Get back to all assets which have been on the homepage

They are in th helm-nagel GDrive ![Link](https://drive.google.com/drive/u/0/folders/1lNymMlPZR1fOZdIcm5kHy44II8B2QlKe)

## Let's Encrypt

Since GitLab Pages support Let's encrypt, `sudo certbot certonly -a manual -d [domain] --email [email]` and 
upload to `" .well-known/acme-challenge"` is no longer needed.

