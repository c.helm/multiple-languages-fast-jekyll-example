---
layout: post
title: Von OCR im Input Management zu Hyperautomation durch KI in Versicherungen
img: analysis.png
caption: Customer Care
excerpt: Deep Learning steigert die Genauigkeit von regelbasierten Ansätzen um 6 % auf 93 %. Zudem sparen Versicherungen Zeit, indem sie intelligente Automatisierungslösungen wie z. B. Hyperautomation einsetzen.
description: Our services enable you to go from images to insights.
date:   2013-12-07 12:00:00
lang: de
ref: input-management
---

KI-getriebenes Input Management durch OCR und NLP
-------------------------------------------------

In Versicherungen ist es längst nichts neues Prozesse über Input Management Systeme zu digitalisieren. Diese Systeme übernehmen eine Verarbeitung des Posteingangs bis zur Archivierung.

Das vorrangige Ziel ist es, Daten strukturiert aufzubereiten, die dann an Folgesysteme, wie z.B. ein ERP-System, weitergegeben werden. Jedoch sind diese Tools oftmals in die Jahre gekommen und sehr kostspielig. Eine Erweiterung des Input Managements durch die Kombination verschiedener Lösungen Künstlicher Intelligenz (KI) wie automatische Texterkennung (OCR) und Textverarbeitung (NLP) wird heutzutage bereits für 62 % der Kundeninteraktionen in Versicherungsunternehmen eingesetzt [1]. Die intelligente OCR nutzt Verschlagwortung und Extraktion von Textfeldern oder ganzen Textabschnitten in Dokumenten oder E-Mails und steigert die Genauigkeit von regelbasierten Ansätzen um 6 % auf 93 %. Zudem sparen Versicherungen Zeit, indem sie intelligente Automatisierungslösungen wie z.B. Hyperautomation einsetzen.


Prozessautomatisierung mit Hyperautomation in der Versicherung
--------------------------------------------------------------

Im Hinblick auf die Pandemie und die daraus resultierende Wirtschaftskrise wird es immer wichtiger, Prozesse in Versicherungen zu optimieren und zu stabilisieren. Durch die Weiterentwicklung von Automatisierungstechnologien wie OCR, RPA (Robotic Process Automation) und KI ergeben sich wirtschaftlich und technologisch fortschrittliche Lösungen zur Prozessautomatisierung - die Hyperautomation. Das Ziel von zahlreichen Unternehmen ist es, die Servicequalität zu erhöhen oder Umsätze zu steigern und bestehende Prozesse noch robuster für die digitale Zukunft des Unternehmens zu gestalten. Durch den Einsatz von Hyperautomation wird eine Automatisierung von Prozessen über regelbasierte Standardanwendungen hinaus ermöglicht.

Automatische Betrugserkennung durch KI in Versicherungen
--------------------------------------------------------

Die Versicherungsbranche hat zunehmend mit Betrugsfällen zu kämpfen, die jährlich Schäden in Milliardenhöhe verursachen. Laut des Gesamtverbands der deutschen Versicherungswirtschaft gehen 10 % der in Deutschland ausgezahlten Schadenleistungen auf die Konten von Betrügern [2]. Um die Betrugsversuche besser zu erkennen, bedarf es technischer Lösungen, die sich stets auf neue Gegebenheiten und Betrugsmuster anpassen können und über regelbasierte Ansätze des Input Managements hinaus gehen. Denn die Fehlerquote ist dort hoch und es wird zusätzlicher manueller Aufwand benötigt. Durch KI und OCR können Schadensmeldungen auf auffällige Inhaltsmuster geprüft werden und Anomalien automatisch erkannt. Mit dem Einsatz von KI konnte bei einer durchschnittlichen Schadenssumme von ca. 3.000 € und der Erkennung von 1.029 Betrugsfällen ein Einsparpotential von über 3,1 Mio. € in einem Versicherungsunternehmen erreicht werden. 

KI in Versicherungen individualisiert die Kundenansprache
---------------------------------------------------------

Individualisierung und Personalisierung gehören zu den Megatrends der 2020er-Jahre. Standardlösungen begeistern den Kunden wenig und die Ansprüche an eine individuelle Kundenansprache steigen. Versicherungen können diese Entwicklung als große Chance für Cross- und Up-selling nutzen, indem sie eine KI-basierte Lösung als Unterstützung einsetzen. Auf Basis von Kundeninformationen lassen sich individuelle E-Mails automatisch generieren und die Qualität der Kommunikation nachhaltig erhöhen. Dabei lassen sich automatisch generierte Texte nicht mehr von händisch erstellten Texten unterscheiden und die Antwortquote von ca. 1,5 % auf bis zu 35 % steigern. Die KI-Anwendung lässt ein automatisches Lernen durch neue Eingaben zu, schließt Wissenslücken und stellt selbstständig neue Verbindungen her. Vortrainierte Sprachmodelle wie z.B. GPT-3 sind leistungsstarke Textgeneratoren, die eigenständig zusammenhängende Texte schreiben und für eine erfolgreiche Kundenansprache genutzt werden [3]. 

Durch KI in Versicherungen Dokumente besser verstehen
-----------------------------------------------------

Die Übertragung von Versicherungsunterlagen zwischen Versicherungen, Maklern und weiteren Partnern ist zwar durch die BiPRO Norm 430 weitgehend standardisiert, jedoch nicht automatisiert [4]. KI verarbeitet Daten in Millionen von Dokumenten und hilft MitarbeiterInnen dabei, Cross-Selling Potenzial in Kundenportfolios zu finden und Geld bei der Vertragsverhandlung und im Input Management zu sparen. Durch den Einsatz von KI können Inhalte in Dokumenten strukturiert abgerufen werden. Arbeitsschritte wie Abtippen, Umbenennen, Ablegen und Validieren entfallen fast vollständig. Dadurch wird es ermöglicht, diese Dokumente rein digital zu verarbeiten, mit bekannten Stammdaten anzureichern und über Systeme hinweg zu harmonisieren. KI Software lernt dabei 24-mal so schnell wie ein Mensch Informationen aus Dokumenten zu verstehen und zu strukturieren. So profitieren Versicherungen von einer schnelleren und effizienteren Verarbeitung von ihren Unterlagen.

### Quellen

[1] [Capgemini Research Institute (2020). Smart Money.](https://www.capgemini.com/de-de/wp-content/uploads/sites/5/2020/11/Report-AI-in-CX-FS.pdf)

[2] [Friedrich, S. (2018). Du Lügst! im Magazin Positionen des GDV, Ausgabe 3/2018, Seiten 24--26.](https://www.gdv.de/resource/blob/40078/d6d85908fca5267199504650a1759dc7/positionen-ausgabe-2018-data.pdf)

[3] [Tan, B., Yang, Z., AI-Shedivat, M., Xing, E. P., & Hu, Z. (2020). Progressive Generation of Long Text.](https://arxiv.org/pdf/2006.15720.pdf)

[4] [BiPRO e.V. (2021). Norm 430. ](https://uebergang.bipro.net/normen/release/release-1/norm/430)

Foto von [Adrianna Calvo](https://www.pexels.com/de-de/@adriannaca?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels) von [Pexels](https://www.pexels.com/de-de/foto/gerader-regenschirm-in-verschiedenen-farben-der-an-schwarzem-draht-hangt-17679/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)



