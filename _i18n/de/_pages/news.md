### Votrag bei appliedAI
##### Am 6. Mai 2021 präsentiert HN als appliedAI Mitglied die neueste KI Software ausgewählten Zuhörern

appliedAI ist Deutschlands größte Initiative für die Anwendung von KI-Technologie mit der Vision, das ganze Land in das
KI-Zeitalter zu heben. appliedAI wurde als neutrale und vertrauenswürdige Initiative gegründet, die sowohl als Plattform
als auch als Dienstleister agiert. Die Helm & Nagel nimmt die Verantwortung als Mitglied ernst und hat das Ziel, jeden einzelnen
Kunden auf die nächste Stufe der KI-Reife zu bringen. Dafür arbeitet die Helm & NAGEL an den technologischen Herausforderungen, sowohl in
der Anwendung als auch in der angewandten Forschung aus strategischer und innovativer Sicht. Wir freuen uns von den
Gründungspartnern wie BMW, Google, Infineon, Linde, NVIDIA und Siemens als Mitglied aufgenommen worden zu sein.  

## News aus unserem Archiv
Bitte wenden Sie sich für journalistische Beiträge per [E-Mail](mailto:info@helm-nagel.com) an uns.

### InsureNXT
##### Input Management 
Als Top 10 InsurTechs präsentiert die Helm & Nagel Gmbh auf der Messe die Input Management Plattform Konfuzio. 
InsureNXT|CGN ist die neue internationale Kongressmesse für Innovation in der Versicherungswirtschaft. Sie ist die
künftig führende Plattform für traditionelle Versicherer, Makler, Startups sowie Beratungsunternehmen. insureNXT zeigt 
branchenübergreifende Lösungen, neue Ökosysteme und Geschäftsmodellen, die die Herausforderungen der digitalen
Transformation und der steigenden Kundenansprüche in der Wirtschaft bewältigen.


### Der Bankingclub berichtet über unsere Software Konfuzio
##### *Eine relevante Lösung für ein vorhandenes Problem*, urteilt der Fachbeirat Robin Nehring

Unsere KI „Konfuzio“ verarbeitet Daten mit Deep Learning. Sie liest relevante Informationen aus Tausenden Seiten aus,
um beispielsweise Abweichungen von Vertragsmustern zu erkennen. Sie ist nicht nur genauer und schneller als Mitarbeiter
 – sie macht auch weniger Fehler. Lesen Sie mehr auf der
[Homepage des Bankingclub](https://www.bankingclub.de/news/fintech-world/konfuzio/){:target="_blank"}

### Die Deutsche Bundesbank wählt Helm & Nagel
##### NLP im Dokumentenaudit

"Bankenaufseher wollen Schieflagen und mögliche Anomalien im Bankensektor schneller und besser erkennen können. Fachleute
aus dem Bereich Finanzstabilität möchten ihre Echtzeitanalysen für Immobilienpreise verbessern, die Volkswirte ihre 
Prognosen für Konjunkturzyklen, die Risiko-Controller ihren Methodenmix und die Statistiker ihre Datenqualität bei der
Erfassung von Fintechs. [...] In einem nächsten Schritt wollen wir gemeinsam überlegen, wie wir die favorisierten Lösungsvorschläge
in der Bundesbank zum Einsatz bringen." Lesen Sie mehr auf der 
[Homepage der Deutschen Bundesbank](https://www.bundesbank.de/de/presse/gastbeitraege/innovation-und-stabilitaet-die-bundesbank-in-der-digitalisierung-861120){:target="_blank"}

### Das Frankfurt Valley berichtet über Helm & Nagel
##### Vorstellung der top 10 Start-ups 

Frankfurt Valley™ ist eine zentrale Plattform zum Entdecken und Interagieren mit hessischen Startups, Unternehmen
Investoren, politischen Entscheidungsträgern und anderen bedeutenden Organisationen. Die Mission von Frankfurt Valley ist 
es, "bi-direktionale Innovationsbrücken zwischen Startup-Ökosystemen weltweit von und nach Frankfurt zu ermöglichen und
zu fördern." Es wurde ursprünglich von Pedro Ferreira gegründet und im September 2020 gingen Frankfurt Valley und Mainstage Incubator ein Joint-Venture ein.
Lesen Sie mehr in dem
[Frankfurt Valley Artikel](https://frankfurtvalley.app/2020/11/24/techquartier-and-deutsche-bundesbank-launch-bundesbank-innovation-challenge/){:target="_blank"}.

### Das Techquartier lädt Helm & Nagel zum Vortrag ein 
##### "Unsupervised Risk Controlling" mit KI

TechQuartier wurde 2016 in Europas Finanzzentrum Frankfurt gegründet und ist eine branchenübergreifende 
Innovationsplattform, die Startups, Unternehmen und neue Talente zusammenbringt, um gemeinsam an neuen Technologien und 
digitalen Geschäftsmodellen zu arbeiten, sich zu treffen, zu lernen und zu kooperieren. Unsere mitgliederbasierte Community
umfasst mehr als 350 Startups, 50 akademische und unternehmerische Innovatoren und Hunderte von potenziellen Gründern.


Lesen Sie auf der [Homepage des Techquartier](https://techquartier.com/news/unsupervised-risk-monitoring/){:target="_blank"} mehr über die
Anwendungsfälle.


### Stark aus eigene Kraft, sagt die Wirtschaftswoche
##### Bericht über unsere Firma

Miriam Binner schreibt "Gründer mit millionenschweren Investments im Rücken galten lange Zeit als Helden der 
Start-up-Szene. Doch die Krise mischt die Karten neu – und zeigt: Nach den eigenen Regeln zu wachsen, hat durchaus 
Vorteile." und spricht für uns damit einen relevanten Punkt an. Es ist im Einzelfall zu entscheiden, ob eine
Finanzierung gegenüber organischem Wachstum bietet.

Lesen Sie mehr auf der
[Homepage der Wirtschaftswoche](https://www.wiwo.de/my/erfolg/gruender/start-up-finanzierung-stark-aus-eigener-kraft/26232920.html){:target="_blank"}

### Die Deutsche Telekom prämiert Helm & Nagel
##### Vorreiter für Digitale Prozesse & Organisation in Deutschland

Wie im Vorjahr vergab die Jury die Auszeichnung an digitale Vorreiter. Ausschlaggebend für die Entscheidung der Jury war
es, ob es den Unternehmen gelungen ist, ihre Produkte mit Hilfe der Digitalisierung zu verbessern. Ob die neue Lösung
dazu beigetragen hat, Prozesse zu optimieren. Oder, wie in der Kategorie „Digitales Kundenerlebnis“, ob die digitalen 
Anwendungen einen Mehrwert für Kunden geschaffen haben. Unternehmen, die in der Kategorie „Digitale Transformation“ 
ausgezeichnet wurden, haben häufig ihr ganzes Geschäftsmodell verändert. 

Lesen Sie mehr auf der
[Homepage der Deutschen Telekom](https://www.telekom.com/de/medien/medieninformationen/detail/digital-x-vier-digitale-vorreiter-aus-der-region-midwest-deutschland-zu-regionalen-digital-champions-gekuert--613220){:target="_blank"}

### Qualitätsmanagement für KI
##### Unser Beitrag für hochwertige KI - Die Informatik Aktuell berichtet
Die DIN 92001 definiert ein Qualitäts-Metamodell über den Lebenszyklus der KI. Die Leistung, Stabilität,
Funktionalität und die Nachvollziehbarkeit stehen im Fokus. Im Vergleich zu herkömmlicher Software ist von der KI 
erlerntes Entscheiden und Handeln jedoch nur selten durch die Sichtung des Quelltexts kontrollierbar. Auch das 800
Seiten umfassende Ergebnis der Enquete-Kommission bestehend aus 19 Abgeordneten und 19 Sachverständigen, das 
am 28. September 2020 im Bundestag vorgestellt wurde, bietet nur erste Ansatzpunkte für den Umgang von KI in der Praxis.
Unser Artikel bietet erprobte Vorgehensweisen aus der Praxis zur Qualitätssicherung bei KI-Modellen während der 
Entwicklung und während des Betriebs, die Praktikern und Entscheidern Optionen für qualitätssichernde Maßnahmen 
aufzeigen, die über die allgemeinen Qualitätssicherungen gemäß ISO 9001 und ISO 12207 hinausgehen.

Lesen Sie mir auf der
[Homepage informatik-aktuell.de](https://www.informatik-aktuell.de/betrieb/kuenstliche-intelligenz/das-magische-dreieck-bei-ki-projekten.html){:target="_blank"}

### Digital Insurance Agenda
##### Digitale Veränderungen gemeinsam gestalten

Gute digitale Lösungen enstehen durch die Abwägung von technischen Möglichkeiten und gelebten Prozessen.
Passende Lösungen für unsere Kunden enstehen in dieser Diskussion. Wir möchten uns hiermit bei allen Besuchern
für Ihre Zeit und die guten Gespräche bedanken. Zum Beispiel für die Möglichkeit
Herrn Dr. Klaus-Peter Röhler, Vorstandsvorsitzender Allianz Deutschland AG, unsere Lösung im Livebetrieb
zu präsentieren, wie auf dem Bild zu sehen.

### Zukunftsrat der Bayerischen Wirtschaft
##### Der Mensch als Lehrer von Konfuzio

Unsere Lösung als Antwort auf die Frage, wie Menschen und Maschinen zusammenarbeiten, um Dokumente besser
zu verstehen und schneller zu verarbeiten. So nutzen wir technologische Innovationen, damit unsere Kunden
sich auf Ihre Kernkompetenz fokussieren können und den Wettbewerb anführen.


### InsurTech Hub Munich am WERK1
##### Top 10 von 160 Bewerbungen anderer Start-ups

Wir sind nun Teil des InsurTech Hub Munich. Der InsurTech Hub München ist eine unternehmerische Plattform,
die Schlüsselpersonen und Disruptoren aus allen Branchen und Technologien anzieht, inspiriert und organisiert,
um gemeinsam an bahnbrechenden, innovativen Versicherungsprodukten und -dienstleistungen zu arbeiten und so
die Zukunft des Versicherungssektors zu revolutionieren. Klicken Sie
[hier](http://www.insurtech-munich.com){:target="_blank"}, um mehr über den InsurTech Hub Munich auf deren
Homepage zu erfahren.


### LMU EC Accelerator
##### Unterstützung erfolgreicher Start-ups

Wir wurden in das Netzwerk des LMU EC Accelerator aufgenommen, zu deren Alumni z. B. Flixbus und andere erfolgreiche Start-ups gehören.
([Link](https://www.entrepreneurship-center.uni-muenchen.de/lmu-ec-accelerator/aktuelle-teams/konfuzio/index.html)){:target="_blank"}.
„Empowering Entrepreneurs!“ ist die Vision des LMU Entrepreneurship Centers (LMU EC), das zum Ziel hat, die
Führungskräfte der Zukunft auszubilden, aktiv die Gründung erfolgreicher Unternehmungen zu
unterstützen und eine Kultur des unternehmerischen Denkens und Handelns zu fördern

### Funding by Microsoft for Startups
##### An exclusive program that delivers best-in-market cloud services to the top startups in the world

Danke an Microsoft für die Unterstützung.

### Andrássy Universität Budapest
##### Vortrag: Transformationspotenzial durch Deep Learning

Inhalt: Das innere Triebwerk digitaler Geschäftsmodelle sind vernetzte Algorithmen, die Unternehmensprozesse
 auf Basis von Informationen steuern. Bei vielen Unternehmen liegen Informationen jedoch in Form von
 unstrukturierten Daten vor, z. B. als PDF, Fax oder E-Mail. Der heute noch händische Zugriff auf Informationen
 erhöht die Stückkosten und lässt innovative Produkte und Services für viele Unternehmen nicht wirtschaftlich
 erscheinen. Die Kompetenz, unstrukturierte Daten durch Deep Learning automatisiert auszuwerten und daraus
 Informationen für Algorithmen zu gewinnen, bietet Wettbewerbsvorteile und Transformationspotenzial. Der
 Vortrag zeigt, wie Unternehmen unstrukturierte Daten auswerten und innovative Produkte und Services
 auch in gewachsenen Unternehmensstrukturen wirtschaftlich betreiben.
 [Read more](https://www.andrassyuni.eu/veranstaltungen/transformationspotenzial-durch-deep-learning.html){:target="_blank"}

### Kooperation Payyxtron
##### Perfektion im Zahlungsverkehr
Die [Payyxtron GmbH](https://www.payyxtron.de/){:target="_blank"} erarbeitet „Perfektion im Zahlungsverkehr“ im Dialog mit Ihren Kunden.
Das Kundenportfolio der Payyxtron GmbH umfasst unterschiedlichste Branchen und Kleine und mittlere Unternehmen (KMU) bis
hin zu Konzernen und Banken.

Im Rahmen einer
Kooperation haben wir ein SAP Add-On entwickelt, das MitarbeiterInnen bei Ihrer täglichen Arbeit
im Zahlungsverkehr sowie im Cash Management unterstützt.
[Hier](https://www.payyxtron.de/mehr-analog-als-digital/){:target="_blank"} gibt es mehr zu lesen.

### Sparkassen Hackathon
##### Das Konzept der digitalen Zettelbox
Auf Basis des Konfuzio Algorithmus entwickelten wir eine Anwendung für kleine und mittelständische
Unternehmen, bei der sich Belege über Auslagen von Mitarbeitern smart erkennen und direkt begleichen lassen.

Beim Sparkassen Hackathon 2018 haben wir einerseits ein Umsetzungsbudget im Gesamtwert von 100.000 Euro gewonnen und
zudem den Sonderpreis in der Kategorie Firmenkunden über 5.000 Euro.
[Hier](https://symbioticon.de/en/){:target="_blank"} mehr lesen.

### Initiative for Industrial Innovators
#### Joint forces for startup excellence in Europe.

Die “Initiative for Industrial Innovators” versteht sich als Programm für die Pre-Seed Finanzierung von
innovativen  Projekten mit einem Schwerpunkt auf industriellen Anwendungsmöglichkeiten. Im Rahmen der
Initiative werden Finanzierungsmittel ausgegeben.

Die Unterstützung haben wir unter anderem genutzt, um die jüngsten Marktanforderungen an B2B-Anwendungsfällen
bei der Verarbeitung von Dokumenten durch eine dynamische Kombination aus Expertenregeln und NLP Regeln zu unterstützen.

### XPRENEURS
#### Incubator for high-tech start-ups in early stages

XPRENEURS ist ein 3-monatiges Vollzeit-Inkubator-Programm mit Sitz in München, das Start-ups in
aller Welt unterstützt, um technologiegetriebene Geschäftsideen in skalierbare Unternehmen zu verwandeln.

Die sehr persönliche Unterstützung durch das XPRENEURS-Team hat uns dabei geholfen, die Möglichkeiten
für den Betrieb der Software zu erweitern und unsere Konformität mit den GDPR Richtlinien noch weiter
zu verbessern.


### ContractGuru
#### Gewinn des Verimi Preises auf dem Bankathon 2018
Nach 2015 haben wir im Jahr 2018 wieder ein Konzept bei dem Bankathon vorgestellt. Eine App,
die Eckdaten von Verträgen mit Hilfe von Konfuzio direkt zusammenfasst und den Vertrag
mit nur einem Klick [Verimi](https://www.verimi.de){:target="_blank"} zum Abschluss bringt.
In Anlehnung an das Frankfurter Start-Up [Finanzguru](https://www.finanzguru.de){:target="_blank"}
haben wir dem Konzept den Namen ContractGuru gegeben.
