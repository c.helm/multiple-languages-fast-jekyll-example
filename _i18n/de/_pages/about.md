## Unsere Firmengeschichte

#### Ein Blick in die Zukunft
Der Bereich Natural Language Processing ist ein gigantischer Wachstumsmarkt. Die Konfuzio Plattform bietet eine einmalige
Plattform, um Textdaten aufzubereiten und spezialisierte NLP Modelle auf Basis von Textdaten zu trainieren, die in den
Archiven von Großunternehmen aktuell nicht genutzt werden. Für diese hoch technische Nutzung baut die Helm & Nagel GmbH
die Vertriebspartnerschaften aus.

#### 2021
Erste Großkunden gehen aktiv auf die Helm & Nagel GmbH zu und wechseln alte OCR oder Input Management Software aus.
Die neuen Möglichkeiten durch Natural Language Processing zeigen wahrnehmbare Erfolge bei den Kunden und setzen neue
Standards in der Dokumentenverarbeitung. Microsoft wird Vertriebspartner.

#### 2020
In 2020 prämierte erst der Hessische Gründerpreis, dann die WirtschaftsWoche und die Deutsche Telekom die Helm & Nagel GmbH
für die OCR & KI Innovationen Konfuzio im Bereich "Digitale Prozesse & Organisation". Konfuzio ist in der 
deutschsprachigen Versicherungswirtschaft schon längst als smarteste Input Management Lösung bekannt.

#### 2019
Das Team der Firma professionalisiert sich in der der Entwicklung von KI Software. Die KI Software Konfuzio entsteht,
deren Funktion erstmals in der Versicherungsbranche im Raum DACH angeboten wird. Gerade diese Branche kennt unter dem 
Namen Input-Management Software und Dunkelverarbeitung bereits die regelbasierten Systeme zum Verarbeiten von 
Dokumenten und E-Mails. Die KI von Konfuzio ist bereits zu diesem Zeitpunkt so smart, dass die vorher händisch 
definierten Regeln nun von der KI gelernt werden. 

#### 2018
Die Firma investiert umfassend in die Professionalisierung der Entwicklung von KI Software. Die Prinzipien der
herkömmlichen Softwareentwicklung reichen nicht aus, um Software zu programmieren, die nach der Übergabe an den Kunden
lernt und sich damit verändert.

#### 2017 
Im Rahmen diverser Kundenprojekte identifiziert das Team der Firma diverse Anwendungsfälle, bei denen KI Software
umfassende Mehrwerte bietet. Durch den fachlichen Hintergrund des Teams im Bereich Quantitativ Finance und 
Risk Controlling sucht das Team vor allem bei Versicherungen und Banken bei Anwendungsfällen von KI. Der Fokus liegt 
dabei auf der Rolle *KI als Enabler*, um eine harmonische Unterstützung von den Kernfunktionen der Bestandskunden
anbieten zu können.

#### 2016
Die Firma wird mit Herrn Helm und Herrn Nagel als Namensgeber gegründet. 

#### 2015

In diesem Jahr berichtet zum ersten mal die Frankfurter Allgemein Zeitung von dem Trio. Unter dem Namen "Eine Nacht mit
den Nerds" berichtet Jonas Jansen von dem Team.

#### 2014

Herr Zyprian, Herr Nagel und Herr Helm treffen sich das erste Mal. Die Freude an der kompetitiven Umsetzung von IT 
verbindet Sie. Programmierwettbewerbe werden für die ersten Ansprachen von möglichen Kunden genutzt. 
Programmierwettbewerbe von Audi, Microsoft, Atos, Siemens, SAP und dem Bankathon werden gewonnen.

#### 2013

Die VBA Kurse und MS Excel Schulungen haben sich im deutschen Markt herumgesprochen. Christopher Helm bietet nun
spezialisierte Kurse für das Investmentbanking und ander Excel Power-User an.

#### 2012
Herr Helm lädt sein erstes [Video](https://www.youtube.com/watch?v=Hd9MiNYyF3U){:target="_blank"} bei YouTube hoch und erklärt
darin, wie man die damals noch funktionierende Yahoo Finance API nutzt, um Kursdaten per VBA in Excel einzufügen.



## Unser Management

#### Chrisotpher Helm
*Kaufmännische Leitung*

Als Digital Native der InsurTech und FinTech-Szene leitet Christopher Helm seit 2016 den KI & OCR Hidden Champion. 
Christopher hat Betriebswirtschaftslehre an der Universität Mannheim und Quantitative Methoden an der TU München 
studiert und entwickelte das "KI-Dreieck" für das kontinuierliche Projektmanagement von künstlichen Intelligenzen.
Den Lebenslauf von Herrn Helm finden Sie auf [LinkedIn](https://www.linkedin.com/in/christopher-helm-public){:target="_blank"}.

#### Florian Zyprian
*Technische Leitung*

Als IT Experte verantwortet Florian Zyprian die Entwicklung und Wartung gemäß Technisch Organisatorischen Maßnahmen.
Als Ansprechpartner für Sicherheit fokussiert er sein Team er sowohl bei der Erstellung der Software als auch bei der
Verarbeitung von Daten auf die die höchsten Maßstäbe. Die Überprüfung auf neueste Sicherheitslücken der eigenen Prozesse
und Software führt immer wieder dazu, dass sein Team auch Sicherheitslücken anderer Unternehmen identifizieren konnte.
Durch dieses Vorgehen konnte bereits diverse DAX Unternehmen auf Verbesserungen hingewiesen werden.
Den Lebenslauf von Herrn Zyprian finden Sie auf [LinkedIn](https://de.linkedin.com/in/zyprian){:target="_blank"}.  