## Next Events

---

### Presentation at appliedAI
#### On May 6, 2021, HN will present the latest AI software to selected audiences as an appliedAI member.

appliedAI is Germany's largest initiative for the application of AI technology with a vision to lift the entire country into the
AI era. appliedAI was founded as a neutral and trustworthy initiative that acts as both a platform and a service
as well as a service provider. Helm & Nagel takes its responsibility as a member seriously and aims to help each and every
customer to the next level of AI maturity. To that end, Helm & Nagel is working to address the technological challenges, both in
application and applied research from a strategic and innovative perspective. We are pleased to be supported by the
founding partners such as BMW, Google, Infineon, Linde, NVIDIA, and Siemens to have accepted HN as a member.  

### Keynote by the Head of AI of HN
#### On May 18, 2021, Official Release of the Konfuzio Python SDK

A very tech-focused coding event where coders, designers and business guys, within a certain time, work on challenges to create innovative solutions. At the end of the event the ideas, and ideally the protoypes will be pitched and judged by a jury. The hackathon takes place completely online. They provide all appropriate tools so that you can stay in touch with your team, other attendees and mentors, even if you are not in the same place. 

[Join the hackathon by Siemens and Zollhof](https://www.hackbay.de/){:target="_blank"} watch the hands on tutorial by Ana
and win up to 1.500 Euros in cash.

## Selected articles in our archive
Please contact us for journalistic contributions via [email](mailto:info@helm-nagel.com).

---

### InsureNXT
#### Input Management 
As one of the top 10 InsurTechs, Helm & Nagel Gmbh presents the input management platform Konfuzio at the fair. 
InsureNXT|CGN is the new international congress fair for innovation in the insurance industry. It is the
future leading platform for traditional insurers, brokers, startups as well as consulting companies. insureNXT shows 
cross-industry solutions, new ecosystems and business models that meet the challenges of digital transformation
and increasing customer demands in the insurance industry.


### Bankingclub reports on our document AI software.
#### *A relevant solution to an existing problem*, judges expert Robin Nehring.

*Our AI "Konfuzio" processes data with Deep Learning. It reads relevant information from thousands of pages, for example, to detect deviations from contract patterns. It is not only more accurate and faster than employees - it also makes fewer mistakes*,
says Christopher Helm.

Read more on the
[Bankingclub homepage](https://www.bankingclub.de/news/fintech-world/konfuzio/){:target="_blank"}

### Deutsche Bundesbank chooses Helm & Nagel
#### NLP in document audit

"Banking supervisors want to be able to detect imbalances and possible anomalies in the banking sector faster and better. Experts
from the area of financial stability would like to improve their real-time analyses for real estate prices, economists their 
forecasts for business cycles, risk controllers their mix of methods, and statisticians their data quality in capturing
coverage of fintechs. [...] In a next step, we want to consider together how we can apply the favored solutions in the Bundesbank.
put to use at the Bundesbank."

Read more on the 
[Deutsche Bundesbank homepage](https://www.bundesbank.de/de/presse/gastbeitraege/innovation-und-stabilitaet-die-bundesbank-in-der-digitalisierung-861120){:target="_blank"}

### Frankfurt Valley reports on Helm & Nagel
#### Helm & Nagel is selected as on of the top 10 start-ups 

Frankfurt Valley™ is a central platform for discovering and interacting with Hessian startups, companies
investors, policy makers and other significant organizations. The mission of Frankfurt Valley is 
to "enable and foster bi-directional innovation bridges between startup ecosystems worldwide to and from Frankfurt.
promote." It was originally founded by Pedro Ferreira, and in September 2020, Frankfurt Valley and Mainstage Incubator entered into a joint venture.

Read more in the
[Frankfurt Valley article](https://frankfurtvalley.app/2020/11/24/techquartier-and-deutsche-bundesbank-launch-bundesbank-innovation-challenge/){:target="_blank"}.

### Techquartier invites Helm & Nagel to speak about AI 
#### "Unsupervised Risk Controlling" with AI.

TechQuartier was founded in 2016 in Europe's financial center Frankfurt and is a cross-industry 
innovation platform that brings together startups, companies and new talent to work together on new technologies and 
digital business models, to meet, learn and collaborate. Our member-based community
includes more than 350 startups, 50 academic and entrepreneurial innovators, and hundreds of potential founders.

Read more on the [Techquartier homepage](https://techquartier.com/news/unsupervised-risk-monitoring/){:target="_blank"} more about the
Use cases.

### *Strong on its own*, says Die WirtschaftsWoche
#### Report about Helm & Nagel

Miriam Binner writes "For a long time, founders with multi-million dollar investments behind them were considered heroes of the 
start-up scene. But the crisis reshuffles the cards" Growing according to one's own rules does provide advantages.
It has to be decided on a case-by-case basis whether financing offers provide advantages over organic growth.

Read more on the
[Wirtschaftswoche homepage](https://www.wiwo.de/my/erfolg/gruender/start-up-finanzierung-stark-aus-eigener-kraft/26232920.html){:target="_blank"}

### Deutsche Telekom honors Helm & Nagel
#### Pioneer for Digital Processes & Organization in Germany

As in the previous year, the jury awarded the prize to digital pioneers. The decisive factor in the jury's decision was
whether the companies had succeeded in improving their offerings with the help of digitization. Whether the new solution
has helped to optimize processes internally. Or, as in the "Digital Customer Experience" category, whether the digital 
applications have created added value for customers. Companies that have won awards in the "Digital Transformation" category 
before needed to change their entire business model. Helm & Nagel was honored as a leading role model in Germany. 

Read more on the
[Deutsche Telekom Homepage](https://www.telekom.com/de/medien/medieninformationen/detail/digital-x-vier-digitale-vorreiter-aus-der-region-midwest-deutschland-zu-regionalen-digital-champions-gekuert--613220){:target="_blank"}

### Quality management for AI
#### Our contribution to high-quality AI - Die Informatik Aktuell reports
DIN 92001 defines a quality metamodel over the lifecycle of AI. The performance, stability,
functionality and traceability are in focus. Compared to conventional software, AI's 
learned decisions and actions can rarely be controlled by reviewing the source code. Even the 800
pages of the Enquete Commission, consisting of 19 members of parliament and 19 experts, which have been 
presented to the Bundestag on September 28, 2020, offers only initial starting points for dealing with AI in practice.
Our article offers best practices from the field for quality assurance in AI models during 
development and operation, providing practitioners and decision makers with options for quality assurance 
that go beyond the general quality assurance requirements of ISO 9001 and ISO 12207.

Read me on the
[Homepage informatik-aktuell.de](https://www.informatik-aktuell.de/betrieb/kuenstliche-intelligenz/das-magische-dreieck-bei-ki-projekten.html){:target="_blank"}

### Digital Insurance Agenda
#### Shaping digital change together

Good digital solutions result from the consideration of technical possibilities and daily processes. Suitable
solutions for our customers emerge in this discussion. We would like to thank all visitors for their time and
good conversations. For example the possibility to seize
Dr. Klaus-Peter R?hler, CEO of Allianz Deutschland AG, to present our solution live while reading documents,
as can be seen in the picture.

### InsurTech Hub Munich at WERK1
#### Top 10 of 160 applications by other Start-Ups

We are now part of the Isuretech Hub in Munich. The InsurTech Hub Munich is an entrepreneurial platform that
attracts, inspires and organizes key players and disruptors across industries and technologies to
collaborate on groundbreaking, innovative insurance products and services ? revolutionizing the future of
Insurance. Find out more on their website, click [here](http://www.insurtech-munich.com){:target="_blank"}
to read more.

### LMU EC Accelerator
#### Part of a startup ecosystem

We were admitted to the LMU EC Accelerator. "Empowering Entrepreneurs" is the vision of the
LMU Entrepreneurship Center (LMU EC), which has the goal of to train the entrepreneurial managers of the
future, to actively promote the foundation of successful companies and to support and promote a culture
of entrepreneurial thinking and action

### Andrássy University Budapest
#### Presentation: Transformation potential through deep learning

Content: The inner engine of digital business models are algorithms that control business processes based on
the information available. However, many companies have information in the form of unstructured data, such as
PDF, fax or e-mail. The still manual access to information increases the unit costs and makes innovative
products and services for many companies not appear economical. The ability to automatically evaluate
unstructured data through deep learning and extract information using algorithms offers competitive advantages
and transformation potential. The presentation shows how companies use unstructured data to serve innovative
products and services even in mature corporate structures. [Read more here](https://www.andrassyuni.eu/veranstaltungen/transformationspotenzial-durch-deep-learning.html){:target="_blank"}

### Cooperation Payyxtron
#### Perfection in payment transactions

The [Payyxtron GmbH](https://www.payyxtron.de/){:target="_blank"} develops "Perfection in payment transactions" in dialogue with their customers.
The customer portfolio of Payyxtron GmbH covers a wide variety of industries and ranges from small to large
medium-sized companies to corporations and banks. In our cooperation, we have developed an SAP Add-On that empowers employees in their daily work
in payment transactions and cash management.

[Read more](https://www.payyxtron.de/mehr-analog-als-digital/){:target="_blank"}

### Sparkassen Hackathon

#### 1st and 2nd place at Symbioticon
Based on the Konfuzio algorithm, we developed an application for small and medium-sized companies
A company that intelligently identifies and settles receipts for employee expenses.

As part of the hackathon we won,  on the one hand, an implementation budget worth a total of € 100,000 and
on the other hand the special prize in the category of corporate customers worth € 5,000.
[Read more](https://symbioticon.de/en/){:target="_blank"}

### Initiative for Industrial Innovators
#### Joins forces for startup excellence in Europe.

The "Initiative for Industrial Innovators" sees itself as a program for the pre-seed financing of
innovative projects with a focus on industrial applications. Among other things, we used the financial support to meet requirements for B2B use cases:
in the processing of documents through a dynamic combination of expert rules and abstract
NLP Rules.

### XPRENEURS
#### Incubator for high-tech start-ups in early stages
XPRENEURS is a 3-month full-time incubator program based in Munich, which supports start-ups around the
world, turning technology-driven business ideas into scalable businesses. The very personal support of the XPRENEURS team helped us to increase the number of our operation models and to further improve our compliance with GDPR.

### ContractGuru
#### 1st place at the Bankathon 2018
After 2015, we again presented a new concept to the Bankathon in 2018. An app,
that summarizes key data of contracts with the help of Konfuzio and let users sign the contract
with just one click using [Verimi](https://www.verimi.de){:target="_blank"}. This signature is legally binding
even under German Law. In addition the process is also accepted by the German Bundesdruckerei.
In reference to the Frankfurt based start-up [Finanzguru](https://www.finanzguru.de){:target="_blank"}
we named our concept ContractGuru.
