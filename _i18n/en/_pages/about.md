## The story behind the company Helm & Nagel GmbH

---

#### A look into the future in 2022
The field of Natural Language Processing is a gigantic growth market. The Konfuzio platform provides a unique
platform to process text data and to train NLP models. Helm & Nagel GmbH is expanding partnerships.

#### 2021
Large customers actively approach Helm & Nagel GmbH and replace old OCR or input management software.
The ROI by Natural Language Processing allows noticeable success among customers. Combined NLP and computer vision is setting new
standards in document processing. Microsoft becomes a sales partner.

#### 2020
In 2020, first the Hessian Founders Award, then WirtschaftsWoche and Deutsche Telekom awarded Helm & Nagel GmbH
for their groundbreaking OCR & AI innovations in the area of "Digital Processes & Organization". Back then their AI Software is already known in the 
German-speaking insurance industry as the smartest input management solution.

#### 2019
The company's team is professionalizing in the development of AI software. The AI software Konfuzio is created available to
selected clients in the insurance industry. Expecially this industry is know for
the long lasting input management history, until now those are rule-based systems for processing documents and e-mails.
In 2019 the AI from Konfuzio is already smart enough that rules which have been defined manually before are now learned by the AI. 

#### 2018
The company is investing extensively in professionalizing the development of AI software. The principles of
traditional software development are not sufficient to software that learns and thus changes after it has been developed
via active learning.

#### 2017 
In the context of various customer projects, the company's team identifies diverse use cases where AI software can
offer comprehensive added value. Due to the team's technical background in quantitative finance and 
risk controlling, the team is primarily looking for AI use cases in insurance companies and banks. Their aim is to put
the role of *AI as an enabler* in order to add harmonious AI support to the core functions of existing customers.

#### 2016
The company is founded with Mr. Helm and Mr. Nagel as name givers. 

#### 2015

In this year the Frankfurter Allgemein Zeitung reports about the trio for the first time. In the article "A night with
the nerds" Jonas Jansen reports about the team.

#### 2014

Mr. Zyprian, Mr. Nagel and Mr. Helm meet for the first time. The joy of the competitive implementation of IT 
connects them. Winning programming competitions were their first customers acquistion strategy. 
Programming competitions of Audi, Microsoft, Atos, Siemens, SAP and the Bankathon are won.

#### 2013

The VBA courses and MS Excel trainings have become popular in the German market. Christopher Helm now offers
specialized courses for investment banking and other Excel power users.

#### 2012
Mr. Helm uploads his first [video](https://www.youtube.com/watch?v=Hd9MiNYyF3U){:target="_blank"} to YouTube and explains
in it how to use the Yahoo Finance API, which was still working at that time, to insert quote data into Excel via VBA.

## The management of Helm & Nagel GmbH

---

#### Chrisotpher Helm
*Commercial Manager*

As an digital native of the InsurTech and FinTech scene Christopher Helm has been leading the AI & OCR hidden champion since 2016. 
Christopher studied Business Administration at the University of Mannheim and Quantitative Methods at the Technical University of Munich 
and developed the "AI Triangle" for continuous project management of artificial intelligences. Feel free to conntect via [LinkedIn](https://www.linkedin.com/in/christopher-helm-public){:target="_blank"}.

#### Florian Zyprian
*Technical Director*

As an IT expert, Florian Zyprian is responsible for the development and maintenance according to Technical Organizational Measures.
As a contact person for security, he focuses his team both in the creation of the software as well as in the
processing of data on the highest standards. Checking for the latest security gaps in the company's own processes
and software led to the fact that his team was also able to identify security vulnerabilities of other companies.
This approach has already enabled him to point out improvements to various DAX companies. Feel free to conntect via  [LinkedIn](https://de.linkedin.com/in/zyprian){:target="_blank"}.  

