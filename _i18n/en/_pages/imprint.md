#### Helm und Nagel GmbH  
#### Rosenweg 5  
#### 35614 Asslar

E-Mail: {{site.email}}

Represented by: Managing Director: Christopher Helm, Florian Zyprian
  

Register:

Register court: Amtsgericht Wetzlar
Sales tax identification number according to §27a sales tax law: DE 306902936
Registration number: HRB 7706

#### Notice on EU Dispute


For extrajudicial settlement of consumer disputes, the European Union has a
Set up an online platform (OS platform) to contact. The platform can be found at
http://ec.europa.eu/consumers/odr/. Our email address is: {{site.email}}

### LEGAL NOTICE


#### § 1 Warning on content

The free and freely accessible contents of this website were created with the greatest possible care. However, the provider of this website assumes no responsibility for the accuracy and timeliness of the provided free and freely accessible journalistic guides and news. Contributions marked by name reflect the opinion of the respective author and not always the opinion of the provider. Just by calling the free and freely accessible content, no contractual relationship between the user and the provider is concluded, insofar as it lacks the legal binding will of the provider.

#### § 2 External links

This website contains links to third party websites ("external links"). These websites are the responsibility of the respective operators. The provider has checked the external content on the initial linking of external links to determine whether any legal violations exist. At that time, no violations were evident. The provider has no influence on the current and future design and content of the linked pages. The setting of external links does not mean that the provider accepts the content behind the link or reference. A constant control of external links is not reasonable for the provider without concrete evidence of legal violations. However, in the event of legal violations, such external links will be deleted immediately.

#### § 3 Copyright and ancillary copyright

The content published on this website is subject to German copyright and ancillary copyright. Any use not permitted by German copyright and ancillary copyright law requires the prior written consent of the provider or respective copyright holder. This applies in particular to duplication, processing, translation, storage, processing or reproduction of content in databases or other electronic media and systems. Contents and rights of third parties are marked as such. The unauthorized duplication or passing on of individual contents or complete sides is not permitted and punishable. Only the production of copies and downloads for personal, private and non-commercial use is permitted. The presentation of this website in external frames is only permitted with written permission.

#### § 4 Special conditions of use

Insofar as special conditions for individual uses of this website deviate from the aforementioned paragraphs, this shall be expressly stated at the appropriate place. In this case, the special conditions of use apply in each individual case.

Source: [Imprint Generator](http://www.juraforum.de/impressum-generator/)

Icon made by Freepik from www.flaticon.com

Icons made by [Prosymbols](http://www.flaticon.com/authors/prosymbols "Prosymbols") from [www.flaticon.com](http://www.flaticon.com "Flaticon") is licensed by [ CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0")

Icons made by [Freepik](http://www.flaticon.com/authors/freepik "Freepik") from [www.flaticon.com](http://www.flaticon.com "Flaticon") is licensed by [ CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0")

Icons made by [Eucalyp](http://www.flaticon.com/authors/eucalyp "Eucalyp") from [www.flaticon.com](http://www.flaticon.com "Flaticon") is licensed by [ CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0")

