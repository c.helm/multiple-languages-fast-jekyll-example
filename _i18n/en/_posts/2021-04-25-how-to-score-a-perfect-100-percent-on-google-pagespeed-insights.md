---
layout: post
title: How to score a perfect 100 percent on Google PageSpeed Insights?
img: analysis.png
caption: Fast Static Page
excerpt: How we used Gzip, jekyll-minifier and other tools to optimized this Jekyll Page hosted via GitLab Pages to score a 100 of 100 Google PageSpeed Insights Score.
description: Our services enable you to go from images to insights.
date: 2021-04-24 12:00:00
lang: en
---

After the rumor started in [2013](https://searchengineland.com/google-mobile-site-speed-162977) Google will add [mobile-friendliness](https://developers.google.com/search/blog/2020/11/timing-for-page-experience) as a page experience signal in a few days. A long debate on "Does page speed correlate with page rank?" exists. Even page speed might [not](https://moz.com/blog/how-website-speed-actually-impacts-search-ranking){:target="_blank"} be correlated with rank, conversion [seems to be](https://www.searchenginewatch.com/2020/11/06/page-speed-is-a-big-deal-why-it-impacts-seo-rankings/){:target="_blank"}. Slow pages might feel like slow IT support, just not appealing. Due to the SEO update by Google our developer's heart has been challenged by the question: *What does it take to update our old homepage and reach a page score of 100?*



![100 Google PageSpeed Insights Score](/img/how_to_score_100.png)

PS: This is our first post on the revised homepage. Feel free to add any feedback to our [HackerNews Post](https://news.ycombinator.com/submitted?id=konfuzio){:target="_blank"}.

## Before

Let me summarize the status quo before we began the optimization:

- We had no contact form
- We did not use the concept of pages in a multilanguage setup
- Individual images on the page were as big as 3 MB
- we used the full bundle of minified Bootstrap files
- the page was optimized for mobile already

This resulted in a Google PageSpeed Score of 51.

## How to improve website speed

The most trivial action included removing any large image or lazy load it via [Lazysize](https://github.com/aFarkas/lazysizes/blob/gh-pages/README.md){:target="_blank"}. Most of the images were rather optional. Funnly enough, our largest image was a picture of our CEO and Dr. Klaus-Peter Röhler, a member of the board at Allianz SE, with more than 3 MB.

<img data-src="/img/roehler_at_dia.jpg" src="/img/roehler_at_dia_small.png" class="lazyload" alt="Dr. Klaus-Peter Röhler at DIA 2019"/>

`<img data-src="/img/roehler_at_dia.jpg" src="/img/roehler_at_dia_small.png" class="lazyload" alt="Dr. Klaus-Peter Röhler at DIA 2019"/>`

However, any further action was even more powerful.

### Add jekyll-minifier to reduce

Make sure to support es6 by adding the following to your _config.yml:

```
jekyll-minifier:
  uglifier_args:
    harmony: true
```
Jekyll-minifier is still using a production variable. This might change according to the issue [55](https://github.com/digitalsparky/jekyll-minifier/issues/55){:target="_blank"}.

`JEKYLL_ENV=production jekyll build -d public`

It minifies HTML, XML, CSS, JSON and JavaScript both inline and as separate files utilising yui-compressor and htmlcompressor.
Also all HTML, JSS and CSS comments will be removed. Below you can find the defaults which are activated per default.

```
  remove_spaces_inside_tags: true   # Default: true
  remove_multi_spaces: true         # Default: true
  remove_comments: true             # Default: true
  compress_css: true                # Default: true
  compress_javascript: true         # Default: true
  compress_json: true               # Default: true
  uglifier_args:                    # harmony
```

This resulted in a minified homepage, see on the right.

![img.png](/img/jekyll_minify.png)

### Use Gzip on Gitlab Pages

If you provide a gz version, GitLab Pages will use the Gzip version. I did not find an official documentation, but a great
guide by [Christian Danscheid](https://webd97.de/post/gitlab-pages-compression/){:target="_blank"}. To summarize, just add
the following to your GitLab CI script.

`&& gzip -k -6 $(find public -type f)`

![See compressed files in Public Gitlab Pages Folder](/img/gzip_compression.png)



### Customize your Bootstrap to improve page speed

Make sure to [customize Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/theming/){:target="_blank"}. Exclude
any element you homepage does not require. Our old homepage used Bootstrap 3, so there was a [page](https://getbootstrap.com/docs/3.4/customize/){:target="_blank"} getbootstrap.com to do so.

### Add-on: Add a Sitemap.xml to a multilanguage Jekyll page


It was pretty hard to find a sitemap generator for multilanguage jekyll pages. Even the plugin `jekyll-sitemap` has marked those feature requests as [wontfix](https://github.com/jekyll/jekyll-sitemap/issues?q=jekyll-multiple-languages-plugin-){:target="_blank"} in the past. This was the reason to combine the
idea by *Independent Sortware* from [Maputo, Mozambique](http://www.independent-software.com/generating-a-sitemap-xml-with-jekyll-without-a-plugin.html){:target="_blank"}
with the `namespace` idea descriped in the [Jekyll Multiple Languages Plugin](https://github.com/kurtsson/jekyll-multiple-languages-plugin#53-permalinks-and-translating-links){:target="_blank"}. Make sure to incluse the urlset attributes so you can use `xhtml:link` tags. As this might be difficult to find, I link to [Stackoverflow](https://stackoverflow.com/questions/36193346/validation-of-xml-sitemap-urlset-with-xhtmllink-inside-url-element#comment83352744_36195612){:target="_blank"} post.

### Congratulations!

You should have reached a Google PageSpeed Insights Score close to 100!


## Why do we write about page optimization?

We just felt challenged! On working days we focus on Computer Vision, NLP, and AI DevOps. Our AI company is bootstrapped since 2016. We mainly focus on AI development and AI DevOps for enterprises within Europe. Today, we just wanted to reach 100 % which is most of the time impossible for AI tasks. Any feedback by SEO or Frontend experts is highly appreciated!

### May we ask you a favor? 

Since 2016 our team invests all resources to finance our development of the Document AI Konfuzio. Konfuzio provides an all in one platform to annotate PDF and
images either manually or via pre-trained models using our Python SDK. Afterward, models to categorize or extract documents can be (re-)train automatically and continuously on new data. More advanced models like text generation via GPT 3 Neo or Textsummarization can be implemented via our Python SDK and CI pipelines. We are looking for Data Engineers and Data Scientists to test our Python SDK before it will be released under MIT License. Do you know someone who **Would like to train NLP models based on document archives or analyze them?** 

Feel free to visit the [Konfuzio Homepage](https://konfuzio.com) or email [Chris](mailto:ch@helm-nagel.com), the guy on the picture.
