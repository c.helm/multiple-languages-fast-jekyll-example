[comment]: <> (---)

[comment]: <> (layout: page)

[comment]: <> (template: portfolio.md)

[comment]: <> (namespace: portfolio)

[comment]: <> (permalink: "/ai-software/")

[comment]: <> (permalink_de: "/ki-software/")

[comment]: <> (title: pages.portfolio.title)

[comment]: <> (description: pages.portfolio.description)

[comment]: <> (order: 2)

[comment]: <> (exclude: true)

[comment]: <> (---)
